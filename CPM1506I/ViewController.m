//
//  ViewController.m
//  CPM1506I
//
//  Created by Michael on 6/7/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//
//  Meds List - iOS Version using Parse

#import "ViewController.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser) {
        NSLog(@"user logged in: %@",currentUser.username);
        
//        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        
//        UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"MedicineList"];
//        [[self navigationController]pushViewController:vc animated:true];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MedicineList"];
        [self.navigationController pushViewController:vc animated:YES];


        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)userSignIn:(id)sender {
    NSLog(@"userSignIn");
    
    NSString *un = self.userName.text;
    NSString *pw = self.password.text;
    
    NSLog(@"\nun: %@ \npw: %@",un,pw);
    
    [PFUser logInWithUsernameInBackground:un password:pw
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                                // Do stuff after successful login.
                                            UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            
                                            UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"MedicineList"];
                                            [[self navigationController]pushViewController:vc animated:true];

                                        } else {
                                                // The login failed. Check error to see why.
                                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"I have a problem."
                                                                                            message:@"Login Failed."
                                                                                           delegate:self
                                                                                  cancelButtonTitle:@"OK"
                                                                                  otherButtonTitles:nil];
                                            [alert show];
                                            
                                            self.userName.text = @"";
                                            self.password.text = @"";
                                            [self.userName becomeFirstResponder];

                                        }
                                    }];

    
    

}

@end
