//
//  MedicalInfoListViewController.h
//  CPM1506I
//
//  Created by Michael on 6/7/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"


@interface MedicalInfoListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *MedName;
@property (weak, nonatomic) IBOutlet UILabel *timesToTake;
@property (weak, nonatomic) IBOutlet UILabel *stillTaking;

@end
