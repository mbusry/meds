//
//  MedicalInfoViewController.h
//  CPM1506I
//
//  Created by Michael on 6/7/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"


@interface MedicalInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *MedName;
@property (weak, nonatomic) IBOutlet UITextField *TimesToTake;
@property (weak, nonatomic) IBOutlet UISwitch *stillTaking;

@end
