//
//  RegisterViewController.m
//  CPM1506I
//
//  Created by Michael on 6/7/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)SaveOnClick:(id)sender {
    
    if([self.password.text isEqualToString: self.reenterPassword.text]) {
    NSLog(@"SaveOnClick");
    PFUser *user = [PFUser user];
    user.username = self.userName.text;
    user.password = self.password.text;
            //ADDED
        PFACL *defaultACL = [PFACL ACL];
        user.ACL = defaultACL;
            //END ADD

    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {   // Hooray! Let them use the app now.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations."
                                                            message:@"Registration is complete."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            
            [alert show];
            UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"AddMedicine"];
            
            [[self navigationController]presentViewController:vc animated:true completion:nil];

            

            
        } else {   NSString *errorString = [error userInfo][@"error"];   // Show the errorString somewhere and let the user try again.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"I have a problem."
                                                            message:@"Passwords did not match."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            
            [alert show];
            self.password.text = @"";
            self.reenterPassword.text = @ "";
            [self.password becomeFirstResponder];
            
            NSLog(@"signup error:\n%@",errorString);

        }
    }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"I have a problem."
                                                        message:@"Passwords did not match."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        self.password.text = @"";
        self.reenterPassword.text = @ "";
        [self.password becomeFirstResponder];

    }

}
- (IBAction)ClearOnClick:(id)sender {
    
    NSLog(@"ClearOnClick");
    
    self.userName.text = @"";
    self.password.text = @"";
    self.reenterPassword.text = @"";
    [self.userName becomeFirstResponder];

}

@end
