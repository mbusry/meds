//
//  MedicalInfoViewController.m
//  CPM1506I
//
//  Created by Michael on 6/7/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//
//SETTING VIEWDIDAPPEAR TO AUTOLOAD

#import "MedicalInfoViewController.h"

@interface MedicalInfoViewController ()
{
    BOOL still;
    NSString *stillText;
    BOOL medItems;
    NSString *currentMedInfoObjectId;
    NSString *med;
    int ttt;
}

@end

@implementation MedicalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    still = YES;
    medItems = NO;
    NSLog(@"still: %i",still);
    
    [self.stillTaking addTarget:self
                      action:@selector(stillTakingSwitch:) forControlEvents:UIControlEventValueChanged];

}

    //copied
-(void)viewWillAppear:(BOOL)animated{
    PFUser *currentUser = [PFUser currentUser];
    
    PFObject *post = currentUser[@"key"];
    
    NSLog(@"key: %@",post.objectId);//  WORKS
    
    NSString *key = post.objectId;
    
    
        // get objid to pull from medInfo
    PFQuery *query = [PFQuery queryWithClassName:@"medInfo"];
    [query getObjectInBackgroundWithId:key block:^(PFObject *medInfo, NSError *error) {
            // Do something with the returned PFObject in the gameScore variable.
        NSLog(@"%@", medInfo);
        
        
        med = medInfo[@"medName"];
        if (med != NULL) {
            medItems = YES;
            ttt = [medInfo[@"timesPerDay"]intValue];
//            BOOL st = [medInfo[@"stillTaking"] boolValue];
            still = [medInfo[@"stillTaking"] boolValue];

            currentMedInfoObjectId = post.objectId;
            NSLog(@"still: %i",still);

            self.MedName.text = med;
            self.TimesToTake.text = [NSString stringWithFormat:@"%d", ttt];
            if (still) {
//                self.stillTaking.text = @"Yes. Still taking this medicine.";
                [_stillTaking setOn:YES animated:YES];
                
            }else{
//                self.stillTaking.text = @"No. Not taking this medicine.";
                [_stillTaking setOn:NO animated:YES];

                
            }
        }
    }];
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)stillTakingSwitch:(UISwitch *) switchState{
    NSLog(@"stillTakingSwitch");
    if (still) {
        NSLog(@"YES now set to NO");
        still = NO;
    }else{
        NSLog(@"NO now set to YES");
        still = YES;
    }
    
    
}

- (IBAction)SaveOnClick:(id)sender {
    
    if (medItems) {
        NSLog(@"SaveOnClick -> medItems");
        NSString *mn = self.MedName.text;
        int tToTake = [self.TimesToTake.text intValue];

        
        
            // Create a pointer to an object of class Point with id dlkj83d
        PFObject *mi = [PFObject objectWithoutDataWithClassName:@"medInfo" objectId:currentMedInfoObjectId];
        
            // Set a new value on quantity
        [mi setObject:mn forKey:@"medName"];
        [mi setObject:@(tToTake) forKey:@"timesPerDay"];
        [mi setObject:[NSNumber numberWithBool:still] forKey:@"stillTaking"];

        [mi saveEventually];
        
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"MedicineList"];
        
        [[self navigationController]pushViewController:vc animated:true];


    }else{
        NSLog(@"SaveOnClick -> !medItems");

    ttt = [self.TimesToTake.text intValue];
    
    NSLog(@"SaveOnClick");
    
    PFUser *currentUser = [PFUser currentUser];
    PFObject *medInfo = [PFObject objectWithClassName:@"medInfo"];
    
    medInfo[@"medName"] = self.MedName.text;
    
    [medInfo setObject:[NSNumber numberWithBool:still] forKey:@"stillTaking"];
    
    medInfo[@"timesPerDay"] = @(ttt);
    
    medInfo.ACL = [PFACL ACLWithUser:[PFUser currentUser]];//original


    [medInfo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
                // The object has been saved.
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save Complete."
                        message:@"Done."
                        delegate:self
                        cancelButtonTitle:@"OK"
                        otherButtonTitles:nil];
            [alert show];
            
            NSLog(@"Medname %@", self.MedName.text);
            NSLog(@"Times Per Day %i", [self.TimesToTake.text intValue]);
            NSLog(@"Still Taking?: %d",still);
            currentUser[@"key"] = medInfo;
            [currentUser saveEventually];
            
            UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"MedicineList"];
            
            [[self navigationController]pushViewController:vc animated:true];

            
        } else {
                // There was a problem, check error.description
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"I have a problem."
                message:@"Save failed."
                delegate:self
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil];
            
            [alert show];
            NSLog(@"Object error: \n%@",error);
  
            
        }
    }];
    
    }

}

- (IBAction)DeleteOnClick:(id)sender {
    NSLog(@"DeleteOnClick");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete"
        message:@"Are you sure?"
        delegate:self
        cancelButtonTitle:@"Cancel"
        otherButtonTitles:@"Yes", nil];
    [alert show];

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
                //this is the "Cancel"-Button
                //do something
            NSLog(@"Cancel");
            self.MedName.text = @"";
            self.TimesToTake.text = @"";
            [self.MedName becomeFirstResponder];

            
        }
            break;
            
        case 1:
        {
                //this is the "Yes"-Button
                //delete object
            
            NSLog(@"Yes");
            
        }
            break;
            
        default:
            break;
    }
    
}


- (IBAction)logoutOnClick:(id)sender {
    NSLog(@"logout");
    [PFUser logOut];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"SignIn"];

    [[self navigationController]presentViewController:vc animated:true completion:nil];
    
    
}


@end
