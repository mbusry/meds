//
//  MedicalInfoListViewController.m
//  CPM1506I
//
//  Created by Michael on 6/7/15.
//  Copyright (c) 2015 Michael Usry. All rights reserved.
//

#import "MedicalInfoListViewController.h"

@interface MedicalInfoListViewController ()

@end

@implementation MedicalInfoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    PFUser *currentUser = [PFUser currentUser];
    
    PFObject *post = currentUser[@"key"];
    
    NSLog(@"key: %@",post.objectId);//  WORKS
    
    NSString *key = post.objectId;
    
    
        // get objid to pull from medInfo
    PFQuery *query = [PFQuery queryWithClassName:@"medInfo"];
    [query getObjectInBackgroundWithId:key block:^(PFObject *medInfo, NSError *error) {
     // Do something with the returned PFObject in the gameScore variable.
        NSLog(@"%@", medInfo);
        
        NSString *med = medInfo[@"medName"];
        if (med != NULL) {
            
        int ttt = [medInfo[@"timesPerDay"]intValue];
        BOOL st = [medInfo[@"stillTaking"] boolValue];

        self.MedName.text = med;
        self.timesToTake.text = [NSString stringWithFormat:@"%d", ttt];
        if (st) {
            self.stillTaking.text = @"Yes. Still taking this medicine.";

        }else{
            self.stillTaking.text = @"No. Not taking this medicine.";

            }
        }
     }];
 

    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)addMed:(id)sender {
    NSLog(@"addMed");
    
    
}
- (IBAction)logoutOnClick:(id)sender {
    NSLog(@"logout");
    [PFUser logOut];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"SignIn"];
    
    [[self navigationController]presentViewController:vc animated:true completion:nil];
    


}

- (IBAction)deleteMed:(id)sender {
    NSLog(@"deleteMed");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Record"
        message:@"Are you sure?"
        delegate:self
        cancelButtonTitle:@"Cancel"
        otherButtonTitles:@"Yes", nil];
    
        // otherButtonTitles is a comma delimited list of strings, terminated with a nil.
    
    [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
                //this is the "Cancel"-Button
                //do something
            NSLog(@"Cancel");

            
        }
            break;
            
        case 1:
        {
                //this is the "OK"-Button
                //do something
            PFUser *currentUser = [PFUser currentUser];
            
            PFObject *post = currentUser[@"key"];
        
            NSLog(@"key: %@",post.objectId);//  WORKS
            
            NSString *key = post.objectId;
            
            PFObject *object = [PFObject objectWithoutDataWithClassName:@"medInfo" objectId:key];
            [object deleteInBackground];
            [currentUser removeObjectForKey:@"key"];

            self.MedName.text = @"";
            self.stillTaking.text = @"";
            self.timesToTake.text = @"";
            
            UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UIViewController *vc = [main instantiateViewControllerWithIdentifier:@"AddMedicine"];
            
            [[self navigationController]presentViewController:vc animated:true completion:nil];

            
            
        }
            break;
            
        case 2:
        {
            NSLog(@"No");
        }
            break;
            
        default:
            break;
    }
    
}


@end
